﻿using Common.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Profiles
{
    public class TaskProfile: AutoMapper.Profile
    {
        public TaskProfile()
        {

            CreateMap<Task, TaskDto>().ReverseMap();
        }
    }
}
