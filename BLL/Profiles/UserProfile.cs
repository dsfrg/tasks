﻿using AutoMapper;
using DAL.Entities;
using Common.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Profiles
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();   
        }
    }
}
