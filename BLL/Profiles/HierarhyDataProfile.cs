﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Profiles
{
    public class HierarhyDataProfile: Profile
    {
        public HierarhyDataProfile()
        {
            CreateMap<HierarchyData, HierarchyDataDto>().ReverseMap();
        }
    }
}
