﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using TaskThreading = System.Threading.Tasks;

namespace BLL.Services
{
    public class UserRoleService
    {
        private readonly IRepository<UserRole> _repository;
        private readonly IMapper _mapper;

        public UserRoleService(IRepository<UserRole> repo, IMapper mapper)
        {
            _repository = repo;
            _mapper = mapper;
        }

        public async TaskThreading.Task AddRole(UserRoleDto item)
        {
            await _repository.Add(_mapper.Map<UserRole>(item));
        }

        public async TaskThreading.Task AddRangeOfRoles(IEnumerable<UserRoleDto> range)
        {
            await _repository.AddRange(_mapper.Map<IEnumerable<UserRole>>(range));
        }

        public async TaskThreading.Task Delete(UserRoleDto item)
        {
            await _repository.Delete(_mapper.Map<UserRole>(item));
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            await _repository.DeleteById(id);
        }

        public async TaskThreading.Task<UserRoleDto> GetUserRole(int id)
        {
            return _mapper.Map<UserRoleDto>(await _repository.GetItem(id));
        }

        public async TaskThreading.Task<IEnumerable<UserRoleDto>> GetUserRoles()
        {
            return _mapper.Map<IEnumerable<UserRoleDto>>(await _repository.GetItems());
        }

        public async TaskThreading.Task Update(UserRoleDto item)
        {
            await _repository.Update(_mapper.Map<UserRole>(item));
        }
    }
}
