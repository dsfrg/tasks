﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThreading = System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService
    {
        private readonly IRepository<Task> _taskRepo;
        private readonly IMapper _mapper;

        public TaskService(IRepository<Task> repo, IMapper mapper)
        {
            _taskRepo = repo;
            _mapper = mapper;
        }

        public async TaskThreading.Task AddTask(TaskDto item)
        {
            await _taskRepo.Add(_mapper.Map<Task>(item));
        }

        public async TaskThreading.Task AddRangeOfTasks(IEnumerable<TaskDto> range)
        {
            await _taskRepo.AddRange(_mapper.Map<IEnumerable<Task>>(range));
        }

        public async TaskThreading.Task Delete(TaskDto item)
        {
            await _taskRepo.Delete(_mapper.Map<Task>(item));
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            await _taskRepo.DeleteById(id);
        }

        public async TaskThreading.Task<TaskDto> GetTask(int id)
        {
            return _mapper.Map<TaskDto>(await _taskRepo.GetItem(id));
        }

        public async TaskThreading.Task<IEnumerable<TaskDto>> GetTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(await _taskRepo.GetItems());
        }

        public async TaskThreading.Task Update(TaskDto item)
        {
            await _taskRepo.Update(_mapper.Map<Task>(item));
        }

        public async TaskThreading.Task<IEnumerable<TaskDto>> GetUnfinishedTaskByUserId(int userId)
        {
            return _mapper.Map<IEnumerable<TaskDto>>((await _taskRepo.GetItems()).Where(x => x.PerformerId == userId && x.State != TaskState.Finished));
        }

        public async TaskThreading.Task MarkTaskAsFinished(TaskDto task)
        {
            task.State = TaskStateDto.Finished;
            await _taskRepo.Update(_mapper.Map<Task>(task));
        }
    }
}
