﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using TaskThreading = System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _teamRepo;
        private readonly IMapper _mapper;

        public TeamService(IRepository<Team> repo, IMapper mapper)
        {
            _teamRepo = repo;
            _mapper = mapper;
        }

        public async TaskThreading.Task AddTeam(TeamDto item)
        {
            await _teamRepo.Add(_mapper.Map<Team>(item));
        }

        public async TaskThreading.Task AddRangeOfTeams(IEnumerable<TeamDto> range)
        {
            await _teamRepo.AddRange(_mapper.Map<IEnumerable<Team>>(range));
        }

        public async TaskThreading.Task Delete(TeamDto item)
        {
            await _teamRepo.Delete(_mapper.Map<Team>(item));
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            await _teamRepo.DeleteById(id);
        }

        public async TaskThreading.Task<TeamDto> GetTeam(int id)
        {
            return _mapper.Map<TeamDto>(await _teamRepo.GetItem(id));
        }

        public async TaskThreading.Task<IEnumerable<TeamDto>> GetTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(await _teamRepo.GetItems());
        }

        public async TaskThreading.Task Update(TeamDto item)
        {
            await _teamRepo.Update(_mapper.Map<Team>(item));
        }
    }
}
