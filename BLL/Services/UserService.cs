﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System.Collections.Generic;
using TaskThreading = System.Threading.Tasks;


namespace BLL.Services
{
    public class UserService
    {
        private readonly IRepository<User> _userRepo;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> repo, IMapper mapper)
        {
            _userRepo = repo;
            _mapper = mapper;
        }

        public async TaskThreading.Task AddUser(UserDto item)
        {
           await _userRepo.Add(_mapper.Map<User>(item));
        }

        public async TaskThreading.Task AddRangeOfUsers(IEnumerable<UserDto> range)
        {
            await _userRepo.AddRange(_mapper.Map<IEnumerable<User>>(range));
        }

        public async TaskThreading.Task Delete(UserDto item)
        {
            await _userRepo.Delete(_mapper.Map<User>(item));
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            await _userRepo.DeleteById(id);
        }

        public async TaskThreading.Task<UserDto> GetUser(int id)
        {
            return _mapper.Map<UserDto>(await _userRepo.GetItem(id));
        }

        public async TaskThreading.Task<IEnumerable<UserDto>> GetUsers()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _userRepo.GetItems());
        }

        public async TaskThreading.Task Update(UserDto item)
        {
            await _userRepo.Update(_mapper.Map<User>(item));
        }
    }
}
