﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public interface IRepository<T>
        where T: class
    {
        Task<T> GetItem(int id);
        Task Add(T item);
        Task AddRange(IEnumerable<T> range);
        Task Update(T item);
        Task Delete(T item);
        Task DeleteById(int id);

        Task<IEnumerable<T>> GetItems();
        Task<IQueryable<T>> GetItemsQueryable();
    }
}
