﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskThreading = System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly InitialDataContext _context;
        public UserRepository(InitialDataContext context)
        {
            _context = context;
        }
        
        public async TaskThreading.Task Add(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");


            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (user != null)
            {
                var nextIndex = _context.Users.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            await _context.Users.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<User> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (await _context.Users.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            await _context.Users.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Users.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<User> GetItem(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            return user;
        }

        public async TaskThreading.Task<IEnumerable<User>> GetItems()
        {
            return await TaskThreading.Task.Run(() => _context.Users);
        }

        public async TaskThreading.Task<IQueryable<User>> GetItemsQueryable()
        {
            return await TaskThreading.Task.Run(() => _context.Users);
        }

        public async TaskThreading.Task Update(User item)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (user == null)
                throw new ArgumentException("id");

            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<User>(user).State = EntityState.Detached;
            _context.Entry<User>(item).State = EntityState.Detached;

            _context.Users.Update(item);
            await _context.SaveChangesAsync();
        }

        
    }
}
