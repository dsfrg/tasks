﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThreading = System.Threading.Tasks;

namespace DAL.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly InitialDataContext _context;
        public ProjectRepository(InitialDataContext context)
        {
            _context = context;
        }
        public async TaskThreading.Task  Add(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = await _context.Projects.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (project != null)
            {
                var nextIndex = _context.Projects.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Projects.Add(item);
             await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<Project> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach(var item in range)
            {
                if (await _context.Projects.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            await _context.Projects.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Projects.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<Project> GetItem(int id)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            return project;
            
        }

        public async TaskThreading.Task<IEnumerable<Project>> GetItems()
        {
            return await _context.Projects.ToListAsync();
        }

        public async TaskThreading.Task<IQueryable<Project>> GetItemsQueryable()
        {
            return await TaskThreading.Task.Run(() =>  _context.Projects);
        }

        public async TaskThreading.Task Update(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = await _context.Projects.FirstOrDefaultAsync(x => x.Id == item.Id);
            if(project is null)
                throw new ArgumentException("item");

            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<Project>(project).State = EntityState.Detached;
            _context.Entry<Project>(item).State = EntityState.Detached;

            _context.Projects.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}
