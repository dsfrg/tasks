﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
