﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class UserTasksDto
    {
        public UserDto User { get; set; }
        public IEnumerable<TaskDto> Tasks{ get; set; }
    }
}
