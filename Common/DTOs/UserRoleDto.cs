﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class UserRoleDto
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public ICollection<UserDto> Users { get; set; }
    }
}
