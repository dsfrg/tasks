﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Common.DTOs.QueryDtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private readonly QueryService _queryService;

        public QueryController(QueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<HierarchyDataDto>>> GetCombinedData()
        {
            return Ok(await _queryService.GetCombinedDataStructureDto());
        }

        [HttpGet("projecttotask/{userId}")]
        public async TaskThreading.Task<ActionResult<IEnumerable<ProjectToTaskCountStructureDto>>> GetProjectWithTaskCountDictionary(int userId)
        {
            return Ok(await _queryService.GetDictionaryProjectToTaskCountByUserId(userId));
        }

        [HttpGet("usertaknameless45/{userId}")]
        public async TaskThreading.Task<ActionResult<IEnumerable<TaskDto>>> GetTasksByUserIdWhereNameLessThan45(int userId)
        {
            return Ok(await _queryService.GetTasksByUserIdWhereNameLessThan45(userId));
        }

        [HttpGet("finishedtasksidname/{userId}")]
        public async TaskThreading.Task<ActionResult<IEnumerable<TasksIdNameDto>>> GetFromCollectionOfTasksWhichAreFinished(int userId)
        {
            return Ok( await _queryService.GetFromCollectionOfTasksWhichAreFinished(userId));
        }


        [HttpGet("teamswithusers")]
        public async TaskThreading.Task<ActionResult<IEnumerable<TeamUsersDto>>> GetTeamsOlderThan10YearsPlusUsers()
        {
            return Ok(await _queryService.GetTeamsOlderThan10Years());
        }

        [HttpGet("userswithtasks")]
        public async TaskThreading.Task<ActionResult<IEnumerable<UserTasksDto>>> GetListOfUsersWithTasks()
        {
            return Ok(await _queryService.GetListOfUsers());
        }

        [HttpGet("infoaboutuser/{userId}")]
        public async TaskThreading.Task<ActionResult<CombinedInfoAboutUserTask6Dto>> GetInfoAboutUser(int userId)
        {
            return Ok(await _queryService.GetInfoAboutUser(userId));
        }

        [HttpGet("GetProjectInfo")]
        public async TaskThreading.Task<ActionResult<IEnumerable<CombinedInfoAboutProjectTask7Dto>>> GetProjectInfo()
        {
            return Ok(await _queryService.GetProjectInfo());
        }

    }
}
