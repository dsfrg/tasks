﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<UserDto>>> GetAllUsers()
        {
            return Ok(await _userService.GetUsers());
        }

        [HttpGet("{id}")]
        public async TaskThreading.Task<ActionResult<UserDto>> GetSingleUser(int id)
        {
            return Ok(await _userService.GetUser(id));
        }


        [HttpPost]
        public async TaskThreading.Task<IActionResult> AddSingleUser(UserDto user)
        {
            await _userService.AddUser(user);
            return Ok();
        }

        [HttpPost("range")]
        public async TaskThreading.Task<IActionResult> AddUsers(IEnumerable<UserDto> users)
        {
            await _userService.AddRangeOfUsers(users);
            return Ok();
        }

        [HttpDelete]
        public async TaskThreading.Task<IActionResult> DeleteUser(UserDto user)
        {
            await _userService.Delete(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async TaskThreading.Task<IActionResult> DeleteUser(int id)
        {
            await _userService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public async TaskThreading.Task<IActionResult> ChangeUser(UserDto user)
        {
            await _userService.Update(user);
            return Ok();
        }
    }
}
