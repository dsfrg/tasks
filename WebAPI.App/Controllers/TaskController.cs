﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<TaskDto>>> GetAllTasks()
        {
            return Ok(await _taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public async TaskThreading.Task<ActionResult<TaskDto>> GetSingleTask(int id)
        {
            return Ok(await _taskService.GetTask(id));
        }


        [HttpPost]
        public async TaskThreading.Task<IActionResult> AddSingleTAsk(TaskDto task)
        {
            await _taskService.AddTask(task);
            return Ok();
        }

        [HttpPost("range")]
        public async TaskThreading.Task<IActionResult> AddTasks(IEnumerable<TaskDto> tasks)
        {
            await _taskService.AddRangeOfTasks(tasks);
            return Ok();
        }

        [HttpDelete]
        public async TaskThreading.Task<IActionResult> DeleteTask(TaskDto task)
        {
            await _taskService.Delete(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async TaskThreading.Task<IActionResult> DeleteTaskById(int id)
        {
            await _taskService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public async TaskThreading.Task<IActionResult> ChangeTask(TaskDto task)
        {
            await _taskService.Update(task);
            return Ok();
        }

        [HttpPut("markFinished")]
        public async TaskThreading.Task<IActionResult> MarkTaskAsFinished(TaskDto task)
        {
            await _taskService.MarkTaskAsFinished(task);
            return Ok();
        }

        [HttpGet("AllUnfinishedTask/{userId}")]
        public async TaskThreading.Task<ActionResult<IEnumerable<TaskDto>>> GetUnfinishedTasksByUserId(int userId)
        {
            return Ok(await _taskService.GetUnfinishedTaskByUserId(userId));
        }
    }
}
