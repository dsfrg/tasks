﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace ConsoleClient.Services
{
    public class TimerService
    {
        private readonly Timer timer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed
        {
            add
            {
                timer.Elapsed += value;
            }

            remove
            {
                timer.Elapsed -= value;
            }
        }



        public TimerService(double interval)
        {
            timer = new Timer();
            Interval = interval;
            timer.Interval = Interval;
            timer.AutoReset = false;
        }
        public void StartReadout()
        {
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }

    }

}
