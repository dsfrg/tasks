﻿using ConsoleClient.Services;
using LINQ_FirstLecture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            MainWindow m = new MainWindow();
            await m.Start();
            Console.Read();
        }
    }
}


